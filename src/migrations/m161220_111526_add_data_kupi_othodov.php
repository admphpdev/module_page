<?php

use yii\db\Migration;
use amd_php_dev\module_page\models\PageOption;
use amd_php_dev\module_page\models\PageOptionGroup;

class m161220_111526_add_data_kupi_othodov extends Migration
{
    public $pageTableName = '{{%page}}';

    public function up()
    {
        $this->insert(
            $this->pageTableName,
            [
                'active' => \amd_php_dev\yii2_components\models\Page::ACTIVE_ACTIVE,
                'name' => 'Контакты',
                'url' => 'contact',
                'text_full' => 'Добро пожаловать',
                'meta_title' => 'Контакты'
            ]
        );

        $this->insert(
            $this->pageTableName,
            [
                'active' => \amd_php_dev\yii2_components\models\Page::ACTIVE_ACTIVE,
                'name' => 'Доставка',
                'url' => 'deliver',
                'text_full' => 'Добро пожаловать',
                'meta_title' => 'Доставка'
            ]
        );

        $this->insert(
            $this->pageTableName,
            [
                'active' => \amd_php_dev\yii2_components\models\Page::ACTIVE_ACTIVE,
                'name' => 'О компании',
                'url' => 'about',
                'text_full' => 'Добро пожаловать',
                'meta_title' => 'О компании'
            ]
        );

        // Группа характеристик - MAIN
        if (!$main = PageOptionGroup::find()->where("code = 'MAIN'")->one()) {
            $main = new PageOptionGroup();
            $main->name = 'Общие характеристики';
            $main->code = 'MAIN';
            $main->all = 1;
            $main->active = PageOptionGroup::ACTIVE_ACTIVE;
            $main->save();
        }

        if ($main && !$option = PageOption::find()->where("code = 'CONTACT_MAP'")->one()) {
            $option = new PageOption();
            $option->id_group = $main->id;
            $option->active = PageOption::ACTIVE_ACTIVE;
            $option->name = 'Код яндекс карты';
            $option->code = 'CONTACT_MAP';
            $option->{PageOption::ATTR_PAGES} = ['contact'];
            $option->save();
        }
        unset($option);

        if ($main && !$option = PageOption::find()->where("code = 'MAIN_VIDEO'")->one()) {
            $option = new PageOption();
            $option->id_group = $main->id;
            $option->active = PageOption::ACTIVE_ACTIVE;
            $option->name = 'Видео на главной';
            $option->code = 'MAIN_VIDEO';
            $option->{PageOption::ATTR_PAGES} = ['main'];
            $option->save();
        }
        unset($option);
    }

    public function down()
    {
        echo "m161220_111526_add_data_kupi_othodov cannot be reverted.\n";

        return false;
    }
}
