<?php
use amd_php_dev\yii2_components\migrations\generators\Option;
use amd_php_dev\yii2_components\migrations\generators\OptionValue;
use amd_php_dev\yii2_components\migrations\generators\OptionGroup;

class m161215_130558_create_page_options extends \amd_php_dev\yii2_components\migrations\Migration
{
    public static $pageOptionTableName           = '{{%page_option}}';
    public static $pageOptionToUrlTableName      = '{{%page_option_to_url}}';
    public static $pageOptionValueTableName      = '{{%page_option_value}}';
    public static $pageOptionGroupTableName      = '{{%page_option_group}}';
    public static $pageOptionGroupToUrlTableName = '{{%page_option_group_to_url}}';

    public function up()
    {
        $generator = new Option($this, self::$pageOptionTableName);
        $generator->additionalColumns['id_group']    = $this->integer();
        $generator->addIndex('id_group');
        $generator->additionalColumns['all'] = $this->boolean()->defaultValue(false);
        $generator->addIndex('all');
        $generator->create();

        $this->createTable(self::$pageOptionToUrlTableName, [
            'id_item' => \yii\db\Schema::TYPE_INTEGER . ' NOT NULL',
            'url' => \yii\db\Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->addPrimaryKey('', self::$pageOptionToUrlTableName, ['id_item', 'url']);

        $generator = new OptionValue($this, self::$pageOptionValueTableName);
        $generator->create();

        $generator = new OptionGroup($this, self::$pageOptionGroupTableName);
        $generator->additionalColumns['all'] = $this->boolean()->defaultValue(false);
        $generator->addIndex('all');
        $generator->create();

        $this->createTable(self::$pageOptionGroupToUrlTableName, [
            'id_item' => \yii\db\Schema::TYPE_INTEGER . ' NOT NULL',
            'url' => \yii\db\Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->addPrimaryKey('', self::$pageOptionGroupToUrlTableName, ['id_item', 'url']);
    }

    public function down()
    {
        $this->dropTable(self::$pageOptionTableName);
        $this->dropTable(self::$pageOptionValueTableName);
        $this->dropTable(self::$pageOptionGroupTableName);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
