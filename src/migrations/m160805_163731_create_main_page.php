<?php

use yii\db\Migration;

class m160805_163731_create_main_page extends Migration
{
    public $pageTableName = '{{%page}}';

    public function up()
    {
        $this->insert(
            $this->pageTableName,
            [
                'active' => \amd_php_dev\yii2_components\models\Page::ACTIVE_ACTIVE,
                'name' => 'Главная',
                'url' => 'main',
                'text_full' => 'Добро пожаловать',
                'meta_title' => 'Главная'
            ]
        );
    }

    public function down()
    {
        echo "m160805_163731_create_main_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
