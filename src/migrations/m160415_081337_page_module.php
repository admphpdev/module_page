<?php

use \amd_php_dev\yii2_components\migrations\generators\Page;

class m160415_081337_page_module extends \amd_php_dev\yii2_components\migrations\Migration
{
    public $pageTableName = '{{%page}}';

    public function up()
    {
        // Создание таблицы страниц
        $pageGenerator = new Page($this, $this->pageTableName);
        $pageGenerator->create();
    }

    public function down()
    {
        $this->dropTable($this->pageTableName);
    }
}
