<?php

namespace amd_php_dev\module_page\controllers;

use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package amd_php_dev\module_page\controllers
 * @property view amd_php_dev\yii2_components\views\View
 */
class DefaultController extends \amd_php_dev\yii2_components\controllers\PublicController
{
    
    public function actionIndex($url)
    {
        // Получаем страницу
        $pageData = \amd_php_dev\module_page\models\Page::find()->active()->url($url)->one();

        if (!$this->view->hasMetaData()) {
            $this->view->setMetaData(
                $pageData->name,
                $pageData->meta_title,
                $pageData->meta_description,
                $pageData->text_full
            );
        }

        $this->view->params['main'] = true;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => $pageData->meta_keywords]);


        return $this->render(
            'index',
            [
                'pageData' => $pageData,
            ]
        );
    }

    protected function deleteManagerData()
    {
        if (\yii::$app->session->has('manager_data')) {
            \yii::$app->session->remove('manager_data');
        }
    }
}
