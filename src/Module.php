<?php

namespace amd_php_dev\module_page;

class Module extends \amd_php_dev\yii2_components\modules\Module implements \yii\base\BootstrapInterface
{
    use \amd_php_dev\yii2_components\modules\ComposerModuleTrait;

    public $controllerNamespace = 'amd_php_dev\module_page\controllers';

    public function init()
    {
        parent::init();
        $this->modules = [
            'admin' => [
                'class' => '\amd_php_dev\module_page\modules\admin\Module',
            ],
        ];
    }
}
