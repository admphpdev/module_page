<?php

namespace amd_php_dev\module_page\models;

/**
 * This is the ActiveQuery class for [[PageOptionGroup]].
 *
 * @see PageOptionGroup
 */
class PageOptionGroupQuery extends \amd_php_dev\yii2_components\models\OptionGroupQuery
{
    use PageOptionAndGroupQueryTrait;

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'pagesManager' => [
                'class' => \amd_php_dev\yii2_components\behaviors\taggable\TaggableQueryBehavior::className(),
                'tagRelation' => 'pagesRelation',
                'tagValueAttribute' => 'url',
            ],
        ]);
    }

    /**
     * @inheritdoc
     * @return PageOptionGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PageOptionGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
