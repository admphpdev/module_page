<?php

namespace amd_php_dev\module_page\models;

use Yii;

/**
 * This is the model class for table "{{%page_option_group}}".
 *
 * @property integer $id
 * @property integer $active
 * @property integer $priority
 * @property integer $id_parent
 * @property string $code
 * @property integer $all
 * @property string $image
 * @property string $name
 */
class PageOptionGroup extends \amd_php_dev\yii2_components\models\OptionGroup
{
    const IMAGES_URL_ALIAS = '@web/data/page/option-group/images/';

    const ALL_TRUE = 1;
    const ALL_FALSE = 0;

    const ATTR_PAGES = 'pages';

    public static function getAllArray()
    {
        return [
            static::ALL_TRUE    => 'Да',
            static::ALL_FALSE   => 'Нет',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_option_group}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'pagesManager' => [
                'class' => \amd_php_dev\yii2_components\behaviors\taggable\TaggableBehavior::className(),
                'tagRelation' => 'pagesRelation',
                'tagValueAttribute' => 'url',
                'tagValueType' => 'string',
                'tagFrequencyAttribute' => false,
                'tagValuesAttribute' => static::ATTR_PAGES,
            ],
        ]);
    }

    /**
    * @inheritdoc
    */
    public static function getActiveArray()
    {
        //return \yii\helpers\ArrayHelper::merge(parent::getActiveArray(), [
        //
        //]);
        return parent::getActiveArray();
    }

    /**
    * @inheritdoc
    */
    public function getItemUrl() {
        if ($this->isNewRecord)
            return false;

        //return Url::to(['', 'url' => $this->url]);
        return '';
    }

    /**
    * @inheritdoc
    */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'all':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_REGULAR_SELECT;
                break;
            case self::ATTR_PAGES:
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_CATEGORIES;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
    * @inheritdoc
    */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'all':
                $result = static::getAllArray();
                break;
            case self::ATTR_PAGES:
                $result = [];

                $data = $this->getPagesRelation()->clean()->asArray()->all();

                foreach ($data as $item) {
                    $result[$item['url']] = $item['url'] . ' - ' . $item['name'];
                }
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    /**
    * @inheritdoc
    */
    public function getInputOptions($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputOptions($attribute);
        }

        return $result;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return \yii\helpers\ArrayHelper::merge(parent::rules(), [
            ['all', 'default', 'value' => static::ALL_FALSE],
            ['all', 'integer'],
            [[self::ATTR_PAGES], 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
            'all' => 'Для всех',
            self::ATTR_PAGES => 'url страниц',
        ]);
    }

    /**
     * @inheritdoc
     * @return PageOptionGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PageOptionGroupQuery(get_called_class());
    }

    public function getPagesRelation()
    {
        return $this->hasMany(Page::className(), ['url' => 'url'])
            ->viaTable('{{%page_option_group_to_url}}', ['id_item' => 'id']);
    }
}
