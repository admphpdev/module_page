<?php
/**
 * Created by PhpStorm.
 * User: v-11
 * Date: 20.12.2016
 * Time: 11:43
 */

namespace amd_php_dev\module_page\models;

/**
 * Class PageOptionAndGroupQueryTrait
 * @package amd_php_dev\module_page\models
 */
trait PageOptionAndGroupQueryTrait
{
    public function getByParams($params = [])
    {
        /**
         * @var $this PageOptionQuery|PageOptionGroupQuery
         */
        if (!empty($params['urls'])) {
            $this->getBehavior('pagesManager')
                ->anyTagValues($params['urls'], 'url', 'LEFT JOIN');
        }

        $this->orWhere($this->getModelTableName() . '.all = 1');

        return $this;
    }
}