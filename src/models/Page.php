<?php

namespace amd_php_dev\module_page\models;

use Yii;
use \amd_php_dev\yii2_components\behaviors\ImageUploadBehavior;
use \yii\helpers\ArrayHelper;
use amd_php_dev\yii2_components\widgets\form\SmartInput;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $priority
 * @property integer $active
 * @property integer $author
 * @property string $name
 * @property string $url
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $text_small
 * @property string $text_full
 * @property string $image_small
 * @property string $image_full
 */
class Page extends \amd_php_dev\yii2_components\models\Page
{

    const IMAGES_PATH_ALIAS = '@webroot/data/images/page/';
    const IMAGES_URL_ALIAS = '@web/data/images/page/';

    const ATTR_OPTIONS = 'optionValues';

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'optionsManager' => [
                'class' => \amd_php_dev\yii2_components\behaviors\OptionBehavior::className(),
                'setableAttribute' => static::ATTR_OPTIONS,
                'optionsRelation' => 'optionsRelation',
                'optionValuesRelation' => 'optionValuesRelation',
                'optionGroupsRelation' => 'optionGroupsRelation',
            ],
        ]);
    }

    public function getItemUrl() {
        if ($this->isNewRecord)
            return false;

        return \yii\helpers\Url::to(['/page/default/index', 'url' => $this->url]);
    }

    /**
     * @inheritdoc
     */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case static::ATTR_OPTIONS :
                $result = SmartInput::TYPE_OPTIONS;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case static::ATTR_OPTIONS :
                $result = $result = $this->getBehavior('optionsManager');
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [[self::ATTR_OPTIONS], 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            self::ATTR_OPTIONS => 'Характеристики'
        ]);
    }

    /**
     * @inheritdoc
     * @return PageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PageQuery(get_called_class());
    }

    /**
     * @return \amd_php_dev\yii2_components\models\OptionGroupQuery
     */
    public function getOptionGroupsRelation($urls = [])
    {
        if (empty($urls) || !is_array($urls)) {
            $urls = [$this->url];
        }
        $query = PageOptionGroup::find()->getByParams(['urls' => $urls]);

        return $query;
    }

    /**
     * @return \yii\db\Query
     */
    public function getOptionValuesRelation()
    {
        $query = new \yii\db\Query();
        return $query->select('*')
            ->from('{{%page_option_value}}')
            ->where('id_item = ' . (int) $this->id)
            ->indexBy('id_option');
    }

    /**
     * @return \amd_php_dev\yii2_components\models\OptionQuery
     */
    public function getOptionsRelation($urls = [])
    {
        if (empty($urls) || !is_array($urls)) {
            $urls = [$this->url];
        }

        $query = PageOption::find()->getByParams(['urls' => $urls]);

        return $query;
    }
}
