<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \amd_php_dev\yii2_components\widgets\form\SmartInput;

/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_page\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<?= \amd_php_dev\yii2_components\widgets\form\AdminPageFieldSet::widget([
    'model' => $model,
    'form' => $form
]) ?>