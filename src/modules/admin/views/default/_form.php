<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \amd_php_dev\yii2_components\widgets\form\SmartInput;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_page\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Основное',
                'content' => $this->render('_item-fields', ['model' => $model, 'form' => $form]),
                'active' => true
            ],
            [
                'label' => 'Дополнительно',
                'content' => $this->render('_item-options', ['model' => $model, 'form' => $form]),
            ],
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
