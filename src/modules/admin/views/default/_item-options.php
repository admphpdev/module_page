<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \amd_php_dev\yii2_components\widgets\form\SmartInput;

/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_page\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<?php if (!empty($form)) : ?>
<div class="options-set" id="item-options">

    <?php
    $urlFromName = $model->formName() . '[url]';
    $ajaxUrl = \yii\helpers\Url::to(['options-by-params']);
    $itemId = $model->id;

    $script = <<<CODE

$(document).ready(function() {
    $(document).on('change', '[name="{$urlFromName}"]', function() {
        var url = $('[name="{$urlFromName}"]').val();
        var itemId = '{$itemId}';

        var data = {
            'url': url,
            'itemId': itemId,
        };
        $.ajax({
            'url': '{$ajaxUrl}',
            'method': 'POST',
            'data': data,
            'dataType': 'html',
            'success': function(res) {
                $('#item-options').html(res);
            }
        });
    });
});
CODE;

    $this->registerJs($script, \yii\web\View::POS_END);
    ?>
    <?php else :
        $form = new \amd_php_dev\yii2_components\widgets\form\DummyActiveForm();
        $form->options['id'] = 'item-form';
        ?>
    <?php endif; ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => \amd_php_dev\module_page\models\Page::ATTR_OPTIONS,
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?php if (!($form instanceof  \amd_php_dev\yii2_components\widgets\form\DummyActiveForm)) : ?>
</div>
<?php else :
    $form->run();
    ?>
<?php endif; ?>
