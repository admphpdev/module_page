<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_page\models\PageOption */

?>
<div class="page-option-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_group',
            'id',
            'active',
            'priority',
            'in_filter',
            'required',
            'code',
            'image',
            'type',
            'name',
            'variants:ntext',
            'default:ntext',
            'description:ntext',
        ],
    ]) ?>

</div>
