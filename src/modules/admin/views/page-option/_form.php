<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_page\models\PageOption */
/* @var $form yii\widgets\ActiveForm */
$allFormName = $model->formName() . '[all]';
$groupFormName = $model->formName() . '[id_group]';
$pagesFormName = $model->formName() . '[' .
    \amd_php_dev\module_page\models\PageOptionGroup::ATTR_PAGES .
    '][]';
?>

<div class="page-option-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'active',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'priority',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'all',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?php
    $script = <<<SCRIPT
$(document).ready(function() {
    $(document).on('change', '[name="{$allFormName}"]', function() {
        if ($(this).val()) {
            $('[name="{$pagesFormName}"]').val(null);
            $('[name="{$pagesFormName}"]').trigger('change');
            $('[name="{$groupFormName}"]').val(null);
            $('[name="{$groupFormName}"]').trigger('change');
        }
    });
});
SCRIPT;
    $this->registerJs($script, \yii\web\View::POS_END);
    ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => \amd_php_dev\module_page\models\PageOption::ATTR_PAGES,
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?php
    $ajaxUrl = \yii\helpers\Url::to(['page-option/groups-by-pages']);
    $script = <<<CODE
$(document).ready(function() {
    $(document).on('change', '[name="{$pagesFormName}"]', function() {
        var val = $(this).val();
        
        if (val) {
            $('[name="{$allFormName}"]').val(0);
        }
        
        var data = {
            'pages': val
        };
        $.ajax({
            'url': '{$ajaxUrl}',
            'method': 'POST',
            'data': data,
            'dataType': 'json',
            'success': function(res) {
                if (res.length) {
                    var input = $('[name="{$groupFormName}"]');
                    input.html('').select2({data: null});
                    input.select2(
                        {
                            'data': res
                        }
                    );
                }
            }
        });
    });
});
CODE;

    $this->registerJs($script, \yii\web\View::POS_END);
    ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'id_group',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'name',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'code',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'variants',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'image',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
