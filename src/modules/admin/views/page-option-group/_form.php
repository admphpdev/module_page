<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_page\models\PageOptionGroup */
/* @var $form yii\widgets\ActiveForm */

$allFormName = $model->formName() . '[all]';
$pagesFormName = $model->formName() . '[' .
    \amd_php_dev\module_page\models\PageOptionGroup::ATTR_PAGES .
    '][]';
?>

<div class="page-option-group-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'active',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'priority',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'all',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?php
    $script = <<<SCRIPT
$(document).ready(function() {
    $(document).on('change', '[name="{$allFormName}"]', function() {
        if ($(this).val()) {
            $('[name="{$pagesFormName}"]').val(null);
            $('[name="{$pagesFormName}"]').trigger('change');
        }
    });
});
SCRIPT;
    $this->registerJs($script, \yii\web\View::POS_END);
    ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => \amd_php_dev\module_page\models\PageOptionGroup::ATTR_PAGES,
        'label'     => true,
        'form'      => $form,
    ]); ?>


    <?php
    $script = <<<CODE

$(document).ready(function() {
    $(document).on('change', '[name="{$pagesFormName}"]', function() {
        var val = $(this).val();
        if (val) {
            $('[name="{$allFormName}"]').val(0);
        }
    });
});
CODE;

    $this->registerJs($script, \yii\web\View::POS_END);
    ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'name',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'image',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'code',
            'label'     => true,
            'form'      => $form,
        ]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
