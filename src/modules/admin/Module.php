<?php

namespace amd_php_dev\module_page\modules\admin;

class Module extends \amd_php_dev\yii2_components\modules\Admin
{
    public $controllerNamespace = 'amd_php_dev\module_page\modules\admin\controllers';

    public static function getMenuItems() {
        return [
            'section' => 'admin',
            'items' => [
                [
                    'label' => 'Статические страницы',
                    'items' => [
                        ['label' => 'Статические страницы', 'url' => ['/page/admin/default/index']],
                        ['label' => 'Храктеристики', 'url' => ['/page/admin/page-option/index']],
                        ['label' => 'Группы характеристик', 'url' => ['/page/admin/page-option-group/index']],
                    ]
                ]
            ],
        ];
    }

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
