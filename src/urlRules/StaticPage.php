<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 30.11.15
 * Time: 16:14
 */

namespace amd_php_dev\module_page\urlRules;

use yii\web\UrlRuleInterface;
use yii\base\Object;
use amd_php_dev\module_page\models\Page;

class StaticPage extends Object implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {
        if ($route === 'page/default/index' && isset($params['url'])) {
            if ($params['url'] === 'main')
                return $manager->baseUrl;
            else
                return $manager->baseUrl . $params['url'];
        }

        return false;  // this rule does not apply
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        if ($request->hostName != \yii::$app->params['HOST']) {
            return false;
        }

        if ($pathInfo === '') {
            $model = Page::find()->url('main')->one();

            if (!empty($model))
                return ['page/default/index', ['url' => $model->url]];
        }

        if (preg_match('/^([a-z\-\_0-9]+)$/', $pathInfo, $matches)) {
            $model = Page::find()->active()->url($matches[1])->one();

            if (!empty($model))
                return ['page/default/' . $model->url, ['url' => $model->url]];
        }

        return false;  // this rule does not apply
    }
}